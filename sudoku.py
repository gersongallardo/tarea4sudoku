#practico4 Gerson Gallardo
board=[
    [5,3,0,0,7,0,0,0,0],
    [6,0,0,1,9,5,0,0,0],
    [0,9,8,0,0,0,0,6,0],
    [8,0,0,0,6,0,0,0,3],
    [4,0,0,8,0,3,0,0,1],
    [7,0,0,0,2,0,0,0,6],
    [0,6,0,0,0,0,2,8,0],
    [0,0,0,4,1,9,0,0,5],
    [0,0,0,0,8,0,0,7,9],
]


def solve(bo):
    find = find_empty(bo)
    if not find:
        return True
    else:
        fila, col = find

    for i in range(1,10):
        if valid(bo, i, (fila, col)):
            bo[fila][col]= i

            if solve(bo):
                return True
            
            bo[fila][col] = 0
            
    return False

def valid(bo, num, pos):

    #comprobar hilera
    for i in range(len(bo[0])):
        if bo[pos[0]][i] == num and pos[1] != i:
            return False
    #comprobar columna
    for i in range(len(bo)):
        if bo[i][pos[1]] == num and pos[0] != i:
            return False
        
    # comprobar casilla
    casilla_x = pos[1] // 3
    casilla_y = pos[0] // 3

    for i in range(casilla_y*3, casilla_y*3 + 3):
        for j in range(casilla_x*3, casilla_x*3 + 3):
            if bo[i][j] == num and (i,j) != pos:
                return False
    return True


def print_board(bo):
    for i in range(len(bo)):
        if i % 3 == 0 and i != 0:
            print("- - - - - - - - - - - - - -")
        for j in range(len(bo[0])):
            if j % 3 == 0 and j != 0:
                print(" | ", end="")
            if j == 8:
                print(bo[i][j])
            else:
                print(str(bo[i][j])+" ", end="")

def find_empty(bo):
    for i in range(len(bo)):
        for j in range(len(bo[0])):
            if bo[i][j] == 0:
                return (i,j)  #fila, col
    return None
print("sudoku de la tabla en la figura 2")
print_board(board)
solve(board)
print("sudoku completado")
print_board(board)
